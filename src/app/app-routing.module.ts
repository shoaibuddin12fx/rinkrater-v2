import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full',
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./pages/search/search.module').then((m) => m.SearchPageModule),
  },
  {
    path: 'main',
    loadChildren: () =>
      import('./pages/main/main.module').then((m) => m.MainPageModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthPageModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about/about.module').then((m) => m.AboutPageModule),
  },
  {
    path: 'add-review',
    loadChildren: () => import('./pages/add-review/add-review.module').then( m => m.AddReviewPageModule)
  },
  {
    path: 'add-rink',
    loadChildren: () => import('./pages/add-rink/add-rink.module').then( m => m.AddRinkPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'partners',
    loadChildren: () => import('./pages/partners/partners.module').then( m => m.PartnersPageModule)
  },
  {
    path: 'profile-card',
    loadChildren: () => import('./pages/profile-card/profile-card.module').then( m => m.ProfileCardPageModule)
  },
  {
    path: 'profile-edit',
    loadChildren: () => import('./pages/profile-edit/profile-edit.module').then( m => m.ProfileEditPageModule)
  },
  {
    path: 'profile-favourites',
    loadChildren: () => import('./pages/profile-favourites/profile-favourites.module').then( m => m.ProfileFavouritesPageModule)
  },
  {
    path: 'rink-info-details',
    loadChildren: () => import('./pages/rink-info-details/rink-info-details.module').then( m => m.RinkInfoDetailsPageModule)
  },
  {
    path: 'profile-photos',
    loadChildren: () => import('./pages/profile-photos/profile-photos.module').then( m => m.ProfilePhotosPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
