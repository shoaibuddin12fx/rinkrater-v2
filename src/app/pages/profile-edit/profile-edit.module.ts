import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileEditPageRoutingModule } from './profile-edit-routing.module';

import { ProfileEditPage } from './profile-edit.page';
import { BannerComponent } from 'src/app/components/banner/banner.component';
import { ProfileFormComponent } from 'src/app/components/profile-form/profile-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileEditPageRoutingModule,
  ],
  declarations: [ProfileEditPage, BannerComponent, ProfileFormComponent],
})
export class ProfileEditPageModule {}
