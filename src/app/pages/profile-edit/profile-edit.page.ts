import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.page.html',
  styleUrls: ['./profile-edit.page.scss'],
})
export class ProfileEditPage extends BasePage implements OnInit {
  user: any;
  currentUser: any;

  constructor(private injector: Injector) {
    super(injector);
    this.user = this.nav.getQueryParams()?.user;
    //  this.currentUser = firebaseApp.auth().currentUser;
  }

  ngOnInit() {}

  private saveUser(form): void {
    delete form.password;
    delete form.confirmPassword;

    this.userService.saveUser(this.user.$key, form).subscribe(
      () => {
        this.utilsService.alert('User updated successfully!', 'Update Info');
        this.nav.pop();
      },
      (error) => this.utilsService.alert(`Error: ${error}`, 'Rink Error')
    );
  }

  submit(form) {
    console.log(form);

    if (!!form.confirmPassword && !!form.password) {
      console.log('has confirm and password');
      if (form.confirmPassword == form.password) {
        console.log('is equal');
        this.currentUser.updatePassword(form.password).then(
          () => {
            console.log('password updated!');
            this.saveUser(form);
          },
          (error) => {
            this.utilsService.alert(error.message, 'Password Update failed');
            //  this.firebaseAuth.logout();
          }
        );
      } else
        this.utilsService.alert(
          'Confirm Password should match with the password.',
          'Verification failed'
        );
    } else this.saveUser(form);
  }
}
