import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RinkInfoDetailsPageRoutingModule } from './rink-info-details-routing.module';

import { RinkInfoDetailsPage } from './rink-info-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RinkInfoDetailsPageRoutingModule
  ],
  declarations: [RinkInfoDetailsPage]
})
export class RinkInfoDetailsPageModule {}
