import { Component, ViewChild, Inject, OnInit, Injector } from '@angular/core';
import { NavController, NavParams, Platform, Content } from 'ionic-angular';
import 'whatwg-fetch';
import { Rink } from 'src/app/shared/rink';
import { ReviewsService } from 'src/app/services/review';
import { BadWordsService } from 'src/app/services/bad-words';
import { RatingsService } from 'src/app/services/rating';
import { RinksService } from 'src/app/services/rink';
import { BasePage } from '../base/base.page';
import { FirebaseApp } from '@angular/fire/compat';

declare var window: any;

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.page.html',
  styleUrls: ['./add-review.page.scss'],
})
export class AddReviewPage extends BasePage implements OnInit {
  private $uid: string;
  private actionY: number;
  imageBlob: any;
  rink: Rink;
  review: any;
  rating: any = {};
  key: string;
  score: number;
  category: string;
  comment: string;
  hasToolBar: boolean = true;
  selectOptions = {
    title: 'Select a category',
  };
  categories: Array<{ name: string; value: any }>;

  // @ViewChild(Content) content: Content;
  @ViewChild('actionsContainer') actionsContainer;
  @ViewChild('reviewCategories') reviewCategories;
  @ViewChild('star1') s1;
  @ViewChild('star2') s2;
  @ViewChild('star3') s3;
  @ViewChild('star4') s4;
  @ViewChild('star5') s5;

  constructor(
    private reviewsService: ReviewsService,
    private ratingsService: RatingsService,
    private rinkService: RinksService,
    private badWordsService: BadWordsService,
    private firebaseApp: FirebaseApp,
    private injector: Injector,
    // private camera: Camera
  ) {
    super(injector);
    this.key = this.nav.getQueryParams()?.key;
    this.rink = this.nav.getQueryParams()?.rink || null;
    this.review = this.nav.getQueryParams()?.review || null;

    this.categories = [
      { name: 'First Impressions:', value: 'FIRST IMPRESSIONS' },
      { name: 'Home Team Insights:', value: 'HOME TEAM INSIGHTS' },
      { name: 'Pro Shop:', value: 'PRO SHOP' },
      { name: 'Skate Sharpening:', value: 'SKATE SHARPENING' },
      { name: 'Concessions:', value: 'CONCESSIONS' },
      { name: 'Rink Temperature:', value: 'RINK TEMPERATURE' },
      { name: 'Facility Overview:', value: 'FACILITY OVERVIEW' },
      { name: 'WI-FI:', value: 'WI-FI' },
      { name: 'Live Streaming:', value: 'LIVE STREAMING' },
      { name: 'Ice Conditions:', value: 'ICE CONDITIONS' },
      { name: 'Warm Up Areas:', value: 'WARM UP AREAS' },
      { name: 'Restrooms:', value: 'RESTROOMS' },
      { name: 'Parking:', value: 'PARKING' },
      { name: 'Locker Rooms:', value: 'LOCKER ROOMS' },
      {
        name: 'Seating / Best Viewing Spots:',
        value: 'SEATING / BEST VIEWING SPOTS',
      },
      { name: 'Sled Hockey:', value: 'SLED HOCKEY' },
      { name: "Ref's Room:", value: "REF'S ROOM" },
      { name: 'Roller Hockey:', value: 'ROLLER HOCKEY' },
    ];
  }

  ngOnInit() {}

  ionViewDidLoad() {
    const rect = this.actionsContainer.nativeElement.getBoundingClientRect();
    this.actionY = rect.bottom;

    if (this.review) {
      this.category = this.review.category;
      this.comment = this.review.comment;
      // this.rating = this.review.rating;
      this.$uid = this.review.userId;
    } else {
      this.category = this.nav.getQueryParams()?.category || 'FIRST IMPRESSIONS';
    }

    // this.firebaseAuth.subscribe(
    //   (auth) => {
    //     this.$uid = auth.uid;

    //     if (this.key) {
    //       this.rinkService.getRinkByKey(this.key).subscribe(
    //         (rink) => {
    //           this.rink = rink;
    //           this.getRatingByUserIdAndRinkId(this.$uid, this.rink.$key);
    //         },
    //         (error) => this.utilsService.alert(error)
    //       );
    //     } else this.getRatingByUserIdAndRinkId(this.$uid, this.rink.$key);
    //   },
    //   (error) => this.utilsService.alert(error)
    // );
  }

  ionViewWillEnter() {
    const rect = this.reviewCategories.nativeElement.getBoundingClientRect();
    // this.content.addScrollListener((event) => {
    //   const scrollTop = event.target.scrollTop;

    //   if (scrollTop >= rect.top) this.onToolbarChange(false);
    //   else this.onToolbarChange(true);
    // });
  }

  onAddReviewOnSamePage(category: string) {
    this.category = category;
    // this.content.scrollTo(0, this.actionY, 200);
  }

  private onToolbarChange(status: boolean): void {
    this.hasToolBar = status;
    // this.content.resize();
  }

  ionViewWillLeave(): void {
    // this.content.scrollToTop(500).then(() => {
    //   this.onToolbarChange(true);
    // });
  }

  ionViewDidLeave() {
    this.reset();
  }

  private getRatingByUserIdAndRinkId(userId: string, rinkId: string) {
    this.ratingsService
      .getRatingByUserIdAndRinkId(userId, rinkId)
      .subscribe((data) => {
        // if (!!this.content) this.content.scrollTo(0, this.actionY, 200);

        this.score = data[0].rating;
        this.rating = data[0];
        this.onPanRating();
      }, console.log);
  }

  private reset(): void {
    this.category = '';
    this.comment = '';
    this.review = null;
  }

  private makeFileIntoBlob(imagePath) {
    if (this.platform.is('android')) {
      return new Promise((resolve, reject) => {
        this.platform.ready().then(() => {
          window.resolveLocalFileSystemURL(imagePath, (fileEntry) => {
            fileEntry.file(
              (resFile) => {
                let reader = new FileReader();
                reader.readAsArrayBuffer(resFile);

                reader.onloadend = (evt: any) => {
                  var imgBlob: any = new Blob([evt.target.result], {
                    type: 'image/jpeg',
                  });
                  imgBlob.name = 'sample.jpg';
                  resolve(imgBlob);
                };

                reader.onerror = (evt: any) => {
                  this.utilsService.alert(
                    'Failed file read: ' + evt.toString()
                  );
                  reject(evt);
                };
              },
              (error) => this.utilsService.alert(error)
            );
          });
        });
      });
    } else {
      return window
        .fetch(imagePath)
        .then((response) => {
          return response.blob();
        })
        .then((blob) => {
          return blob;
        })
        .catch((error) => this.utilsService.alert(error));
    }
  }

  private uploadToFirebase(imageBlob) {
    let fileName = 'sample-' + new Date().getTime() + '.jpg';
    return new Promise((resolve, reject) => {
      let fileRef = this.storage.ref(`rinkImages/${this.$uid}/${fileName}`);
      let uploadTask = fileRef.put(imageBlob);

      uploadTask.then((snapshot) => {
        console.log('snapshot progress ' + snapshot);
        let data = {
          snapshot: snapshot,
          fileName: fileName,
        };
        resolve(data);
      }),
        (error) => {
          console.log('Error');
          reject(error.message);
        };
    });
  }

  private getPicture(sourceType: string) {
    // camera options
    // let opts = {
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   sourceType: this.camera.PictureSourceType[sourceType],
    //   cameraDirection: this.camera.Direction.FRONT,
    //   targetHeight: 640,
    //   correctOrientation: true,
    // };

    this.utilsService.showLoading('Loading image path...', 'Get Photo');

    // get picture from camera
    // this.camera
    //   .getPicture(opts)
    //   .then(
    //     (imagePath: any) => {
    //       return this.makeFileIntoBlob(imagePath);
    //     },
    //     (error) => this.onError(error)
    //   )
    //   .then(
    //     (imageBlob: any) => {
    //       this.utilsService.hideLoading();
    //       this.imageBlob = imageBlob;
    //     },
    //     (error) => this.onError(error)
    //   );
  }

  openEmail() {
    if (this.platform.is('cordova')) {
      let email = {
        to: 'senan@rinkrater.com',
        subject: '',
        body: '',
        isHtml: true,
      };
      this.emailComposer.open(email);
    }
  }

  takePhoto() {
    if (this.platform.is('cordova')) {
      this.utilsService
        .confirm('Take a photo or choose from Camera roll?', 'Take Photo', [
          'Take Photo',
          'Camera Roll',
        ])
        .then((idx) => {
          console.log(idx);

          if (idx == 1) this.getPicture('CAMERA');
          else this.getPicture('PHOTOLIBRARY');
        });
    }
  }

  createNewReview(review: any, photo: any = null) {
    let message: string = 'Review added successfully!';
    if (!!this.review) {
      review.$key = this.review.$key;
      message = 'Review updated successfully!';
    }

    this.reviewsService.createNewReview(review, photo).subscribe(
      () => {
        this.utilsService.hideLoading();
        this.utilsService.alert(message, 'Rink Review');
        this.reset();

        if (!!this.review) this.nav.pop();
      },
      (error) => this.onError(error)
    );
  }

  submit() {
    this.utilsService.showLoading('Loading...');

    const timestamp: number = new Date().getTime();
    const review = {
      category: this.category,
      comment: this.badWordsService.clean(this.comment),
      created_at: timestamp,
      created_by: this.$uid,
      updated_at: timestamp,
      updated_by: this.$uid,
      userId: this.$uid,
      rinkId: this.rink.$key,
    };

    if (!!this.imageBlob) {
      this.uploadToFirebase(this.imageBlob).then(
        (data: any) => {
          let photo = {
            url: data.snapshot.downloadURL,
            raw: data.fileName,
            rinkId: review.rinkId,
            rinkName: this.rink.name,
            userId: review.userId,
          };
          this.createNewReview(review, photo);
        },
        (error) => this.onError(error)
      );
    } else {
      console.log('AddReviewPage: createNewReview', this.rating);

      this.rating.rating = this.score;
      this.rating.userId = this.$uid;
      this.rating.rinkId = this.rink.$key;

      this.ratingsService
        .createNewRating(this.rating)
        .subscribe((key: string) => {
          this.rating.$key = key;
          console.log('AddReviewPage: this.ratingsService', this.rating);
          this.createNewReview(review);
        }, console.log);
    }
  }

  onError(error: any) {
    // show error
    this.utilsService.alert(error);
    // hide loading
    this.utilsService.hideLoading();
  }

  onPanRating() {
    if (this.score == 1) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 0.5;
      this.s3.nativeElement.style.opacity = 0.5;
      this.s4.nativeElement.style.opacity = 0.5;
      this.s5.nativeElement.style.opacity = 0.5;
    } else if (this.score == 2) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 0.5;
      this.s4.nativeElement.style.opacity = 0.5;
      this.s5.nativeElement.style.opacity = 0.5;
    } else if (this.score == 3) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = 0.5;
      this.s5.nativeElement.style.opacity = 0.5;
    } else if (this.score == 4) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = 1;
      this.s5.nativeElement.style.opacity = 0.5;
    } else if (this.score == 5) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = 1;
      this.s5.nativeElement.style.opacity = 1;
    }
  }
}
