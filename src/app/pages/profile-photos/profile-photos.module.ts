import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePhotosPageRoutingModule } from './profile-photos-routing.module';

import { ProfilePhotosPage } from './profile-photos.page';
import { SocialComponent } from 'src/app/components/social/social.component';
import { BannerComponent } from 'src/app/components/banner/banner.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePhotosPageRoutingModule
  ],
  declarations: [ProfilePhotosPage, SocialComponent,BannerComponent]
})
export class ProfilePhotosPageModule {}
