import { Component, Injector, OnInit } from '@angular/core';
import { PhotosService } from 'src/app/services/photo';
import { BasePage } from '../base/base.page';
import {
  ActionSheet,
  ActionSheetOptions,
} from '@ionic-native/action-sheet/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import * as _ from 'lodash';

declare var window: any;

@Component({
  selector: 'app-profile-photos',
  templateUrl: './profile-photos.page.html',
  styleUrls: ['./profile-photos.page.scss'],
})
export class ProfilePhotosPage extends BasePage implements OnInit {
  user: any;
  photos: any;
  photosArray: Array<string> = [];
  imageBlob: any;

  constructor(
    private photosService: PhotosService,
    private injector: Injector,
    private actionSheet: ActionSheet,
    private photoViewer: PhotoViewer
  ) {
    super(injector);
    this.user = this.nav.getQueryParams()?.user;
  }

  ngOnInit() {
    // this.utilsService.showLoading('Loading...', 'Rink Photos');
    console.log(this.user.$key);
    this.photosService.getAllPhotosForUser(this.user.$key).subscribe(
      (photos) => {
        console.log('photos');
        // this.utilsService.hideLoading();
        this.photos = photos;
      },
      (err) => {
        console.log('error');
        console.log(err);
        // this.utilsService.hideLoading();
      }
    );
  }

  private makeFileIntoBlob(imagePath) {
    if (this.platform.is('android')) {
      return new Promise((resolve, reject) => {
        this.platform.ready().then(() => {
          window.resolveLocalFileSystemURL(imagePath, (fileEntry) => {
            fileEntry.file(
              (resFile) => {
                let reader = new FileReader();
                reader.readAsArrayBuffer(resFile);

                reader.onloadend = (evt: any) => {
                  var imgBlob: any = new Blob([evt.target.result], {
                    type: 'image/jpeg',
                  });
                  imgBlob.name = 'sample.jpg';
                  resolve(imgBlob);
                };

                reader.onerror = (evt: any) => {
                  this.utilsService.alert(
                    'Failed file read: ' + evt.toString()
                  );
                  reject(evt);
                };
              },
              (error) => this.utilsService.alert(error)
            );
          });
        });
      });
    } else {
      return window
        .fetch(imagePath)
        .then((response) => {
          return response.blob();
        })
        .then((blob) => {
          return blob;
        })
        .catch((error) => this.utilsService.alert(error));
    }
  }

  private uploadToFirebase(imageBlob) {
    var fileName = 'sample-' + new Date().getTime() + '.jpg';
    return new Promise((resolve, reject) => {
      var fileRef = this.storage.ref(
        `rinkImages/${this.user.$key}/${fileName}`
      );

      var uploadTask = fileRef.put(imageBlob);

      uploadTask.then(
        (snapshot) => {
          console.log('snapshot progress ' + snapshot);
          let data = {
            snapshot: snapshot,
            fileName: fileName,
          };
          resolve(data);
        },
        (error) => reject(error.message)
      );
    });
  }

  private comparePhoto(collection: Array<string>, compare: string): boolean {
    return _.some(collection, (photo) => {
      return photo === compare;
    });
  }

  private getPicture(sourceType: string) {
    this.utilsService.showLoading('Loading image path...', 'Get Photo');

    let opts = {
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType[sourceType],
      cameraDirection: this.camera.Direction.FRONT,
      targetHeight: 640,
      correctOrientation: true,
    };

    // get picture from camera
    this.camera
      .getPicture(opts)
      .then(
        (imagePath: any) => {
          return this.makeFileIntoBlob(imagePath);
        },
        (error) => (error) => this.onError(error)
      )
      .then(
        (imageBlob: any) => {
          return this.uploadToFirebase(imageBlob);
        },
        (error) => this.onError(error)
      )
      .then(
        (data: any) => {
          let photo = {
            url: data.snapshot.downloadURL,
            raw: data.fileName,
            rinkId: null,
            rinkName: null,
            userId: this.user.$key,
          };
          this.photosService.createNewPhoto(photo).subscribe(
            () => {
              this.utilsService.hideLoading();
              this.utilsService.alert('File uploaded successfully!');
              this.imageBlob = null;
            },
            (error) => this.onError(error)
          );
        },
        (error) => this.onError(error)
      );
  }

  takePhoto() {
    if (this.platform.is('cordova')) {
      this.utilsService
        .confirm('Take a photo or choose from Camera roll?', 'Take Photo', [
          'Take Photo',
          'Camera Roll',
        ])
        .then((idx) => {
          console.log(idx);

          if (idx == 1) this.getPicture('CAMERA');
          else this.getPicture('PHOTOLIBRARY');
        });
    }
  }

  viewPhoto(photo: any) {
    const url = photo.url;
    const fileName = photo.raw;
    const selectAction = this.comparePhoto(this.photosArray, url)
      ? 'Deselect Photo'
      : 'Select Photo';
    const buttonLabels = [selectAction, 'Preview Photo'];

    this.actionSheet
      .show({
        title: 'Actions',
        buttonLabels: buttonLabels,
        addCancelButtonWithLabel: 'Cancel',
        addDestructiveButtonWithLabel: 'Delete Photo',
      })
      .then((buttonIndex: number) => {
        console.log('Button pressed: ' + buttonIndex);
        switch (buttonIndex) {
          case 1:
            // Delete Photo
            const photoRef = this.storage.ref(
              `rinkImages/${this.user.$key}/${fileName}`
            );
            // Remove from firebase storage
            photoRef.delete().subscribe(() => {
              // Remove from photos object
              this.photosService
                .deletePhoto(photo.$key, this.user.$key)
                .subscribe(() => {
                  // Splice from photosArray
                  this.photosArray.splice(this.photosArray.indexOf(url), 1);
                });
            });
            // .catch((error) => {
            //   this.utilsService.alert(error);
            // });
            break;
          case 2:
            // Select Photo
            if (this.comparePhoto(this.photosArray, url)) {
              this.photosArray.splice(this.photosArray.indexOf(url), 1);
            } else this.photosArray.push(url);
            break;
          case 3:
            // Preview Photo
            this.photoViewer.show(url);
            break;
        }
      });
  }

  isSelected(photo: any) {
    return this.comparePhoto(this.photosArray, photo.url);
  }

  submit() {
    if (!!this.imageBlob) {
      this.utilsService.showLoading('Loading...');
      this.uploadToFirebase(this.imageBlob).then(
        (data: any) => {
          let photo = {
            url: data.snapshot.downloadURL,
            raw: data.fileName,
            rinkId: null,
            rinkName: null,
            userId: this.user.$key,
          };
          this.photosService.createNewPhoto(photo).subscribe(
            () => {
              this.utilsService.hideLoading();
              this.utilsService.alert('File uploaded successfully!');
              this.imageBlob = null;
            },
            (error) => this.onError(error)
          );
        },
        (error) => this.onError(error)
      );
    } else {
      this.utilsService.alert('Image path not available!');
    }
  }

  onError(error: any) {
    // show error
    this.utilsService.alert(error);
    // hide loading
    this.utilsService.hideLoading();
  }
}
