import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileCardPageRoutingModule } from './profile-card-routing.module';

import { ProfileCardPage } from './profile-card.page';
import { SocialComponent } from 'src/app/components/social/social.component';
import { CardFormComponent } from 'src/app/components/card-form/card-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileCardPageRoutingModule,
  ],
  declarations: [ProfileCardPage, SocialComponent, CardFormComponent],
})
export class ProfileCardPageModule {}
