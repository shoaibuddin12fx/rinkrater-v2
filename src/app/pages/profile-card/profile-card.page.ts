import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.page.html',
  styleUrls: ['./profile-card.page.scss'],
})
export class ProfileCardPage extends BasePage implements OnInit {
  user: any;

  constructor(private injector: Injector) {
    super(injector);
    this.user = this.nav.getQueryParams()?.user;
  }

  submit(form: any) {
    console.log(form);

    this.user.parentCard = form;

    this.userService.saveUser(this.user.$key, this.user).subscribe(
      () => {
        this.utilsService.alert('User updated successfully!', 'Update Info');
      },
      (error) => this.utilsService.alert(`Error: ${error}`, 'Rink Error')
    );
  }

  ngOnInit() {}
}
