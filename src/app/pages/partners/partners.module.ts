import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PartnersPageRoutingModule } from './partners-routing.module';

import { PartnersPage } from './partners.page';
import { BannerComponent } from 'src/app/components/banner/banner.component';
import { SocialComponent } from 'src/app/components/social/social.component';
import { PartnerListComponent } from 'src/app/components/partner-list/partner-list.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PartnersPageRoutingModule],
  declarations: [
    PartnersPage,
    BannerComponent,
    SocialComponent,
    PartnerListComponent,
  ],
})
export class PartnersPageModule {}
