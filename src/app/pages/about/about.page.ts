import { Component, Injector, OnInit } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireObject,
} from '@angular/fire/compat/database';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage extends BasePage implements OnInit {
  private previous: number = 0;
  about: any;
  accordion: Array<{ header: string; toggle: boolean; iconRight: string }>;

  constructor(
    private angularFireDatabase: AngularFireDatabase,
    private injector: Injector
  ) {
    super(injector);
    this.accordion = [
      { header: 'What is Rink Rater?', toggle: true, iconRight: 'remove' },
      { header: 'Who Are We?', toggle: false, iconRight: 'add' },
      { header: 'Rink Rater Gear Store?', toggle: false, iconRight: 'add' },
    ];
  }
  ngOnInit() {}

  ionViewDidLoad() {
    this.about = this.angularFireDatabase.object(`about`);
  }

  onNavigateSettings() {
    setTimeout(() => {
      this.nav.push('settings');
    }, 300);
  }

  goSearch() {
    setTimeout(
      () =>
        this.nav.setRoot(
          'search',
          {}
          // { animate: true, direction: 'back' }
        ),
      300
    );
  }

  onToggle(idx: number) {
    this.accordion[idx].toggle = !this.accordion[idx].toggle;
    this.accordion[idx].iconRight =
      this.accordion[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0) this.previous = idx;
    else {
      if (this.previous != idx) {
        this.accordion[this.previous].toggle = false;
        this.accordion[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }
}
