import { NavService } from './../../services/nav.service';
import { Injector } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { Location } from '@angular/common';
import { Platform, MenuController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { FormBuilder } from '@angular/forms';
// import { PopoversService } from 'src/app/services/basic/popovers.service';
// import { UserService } from 'src/app/services/user.service';
// import { ModalService } from 'src/app/services/basic/modal.service';
// import { PermissionsService } from 'src/app/services/permissions.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/toast.service';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { RinksService } from 'src/app/services/rink';
import { UtilsService } from 'src/app/services/utils';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Device } from '@ionic-native/device/ngx';
import { Camera } from '@ionic-native/camera/ngx';

export abstract class BasePage {
  public utility: UtilityService;
  public nav: NavService;
  public location: Location;
  public events: EventsService;
  public platform: Platform;
  public formBuilder: FormBuilder;
  // public popover: PopoversService;
  // public users: UserService;
  // public modals: ModalService;
  public menuCtrl: MenuController;
  // public permissions: PermissionsService;
  public domSanitizer: DomSanitizer;
  public af: AngularFireDatabase;
  // public storedContactsService: StoredContactsService;
  public auth: AuthService;
  public toastService: ToastService;
  public userService: UserService;
  public activatedRoute: ActivatedRoute;
  public storage: AngularFireStorage;
  public afAuth: AngularFireAuth;
  public emailComposer: EmailComposer;
  public rinksService: RinksService;
  public utilsService: UtilsService;
  public camera: Camera;

  public device: Device;

  constructor(injector: Injector) {
    this.platform = injector.get(Platform);
    // this.sqlite = injector.get(SqliteService);
    // this.users = injector.get(UserService);
    // this.network = injector.get(NetworkService);
    this.utility = injector.get(UtilityService);
    this.location = injector.get(Location);
    this.events = injector.get(EventsService);
    this.nav = injector.get(NavService);
    this.formBuilder = injector.get(FormBuilder);
    // this.popover = injector.get(PopoversService);
    // this.modals = injector.get(ModalService);
    this.menuCtrl = injector.get(MenuController);
    // this.permissions = injector.get(PermissionsService);
    this.domSanitizer = injector.get(DomSanitizer);
    this.af = injector.get(AngularFireDatabase);
    this.auth = injector.get(AuthService);
    this.toastService = injector.get(ToastService);
    this.userService = injector.get(UserService);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.storage = injector.get(AngularFireStorage);
    this.emailComposer = injector.get(EmailComposer);
    this.rinksService = injector.get(RinksService);
    this.utilsService = injector.get(UtilsService);
    this.camera = injector.get(Camera);
  }

  goSettings() {
    setTimeout(() => this.nav.push('settings'), 300);
  }
}
