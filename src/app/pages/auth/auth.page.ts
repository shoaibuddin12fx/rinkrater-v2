import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Browser } from '@capacitor/browser';
import { Keyboard } from '@capacitor/keyboard';
import { TERMS_OF_USE } from 'src/app/shared/constants';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage extends BasePage implements OnInit {
  form: FormGroup;
  authType: string;
  firebase: any;
  isAndroid: boolean;

  email: string;

  constructor(injector: Injector) {
    super(injector);
    this.isAndroid = this.platform.is('android');
    this.authType = this.nav.getQueryParams()?.authType;

    console.log('authType:', this.authType);

    let formGroups = {
      login: {
        email: ['', Validators.required],
        password: ['', Validators.required],
      },
      register: {
        fullName: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required],
        confirm: ['', Validators.required],
      },
      reset: {
        email: ['', Validators.required],
      },
    };

    this.form = this.formBuilder.group(formGroups[this.authType]);
    // this.firebase = firebase;
  }

  ngOnInit() {}

  onReset() {
    const form: any = this.form.value;
    const email: string = form.email;

    this.utility.showLoading('Loading... Authenticating');

    // this.auth
    //   .reset(email)
    //   .subscribe(
    //     (response: any) => {
    //       this.utility.hideLoading();
    //       this.utility.alert('Password reset sent to your email address.');
    //       this.nav.setRoot('');
    //     },
    //     (error: any) => {
    //       this.utility.hideLoading();
    //       this.utility.alert(error.message, 'Reset password error');

    //     }
    //   );
  }

  doAuth(ev) {
    Keyboard.hide();

    this.utility.showLoading('Loading... Authenticating');

    const formValue = this.form.value;

    switch (this.authType) {
      case 'login':
        // this.auth
        //   .login(formValue.email, formValue.password)
        //   .subscribe(
        //     (data) => {
        //       console.log('data', data);
        //       this.utility.hideLoading();
        //     },
        //     (error) => {
        //       this.utility.hideLoading();
        //       if (this.platform.is('cordova'))
        //         this.toastService.show(error.message).subscribe();
        //       else {
        //         let toast = this.toastController.create({
        //           message: error.message,
        //           duration: 3000
        //         });
        //         toast.present();
        //       }
        //     }
        //   );
        break;
      case 'register':
        formValue.provider = 'email';
        // this.auth
        //   .signIn(formValue.email, formValue.password)
        //   .subscribe(
        //     (data) => {
        //       this.utility.hideLoading();

        //       let user = this.firebase.auth().currentUser;
        //       user.sendEmailVerification().then(
        //         () => { this.userService.createUser(data.uid, formValue); },
        //         console.log
        //       );

        //         this.toastService.show('User created successfully! WooHoo!')

        //     },
        //     (error) => {
        //       this.utility.hideLoading();
        //       this.toastService.show(error.message);

        //     }
        //   );
        break;
    }
  }

  isPasswordMatch() {
    const val = this.form.value;
    switch (this.authType) {
      case 'login':
        return true;
      case 'register':
        return val && val.password && val.password == val.confirm;
    }
  }

  openTOS(): void {
    this.platform.ready().then(() => Browser.open(TERMS_OF_USE));
  }

  openAuth() {
    setTimeout(() => {
      if (this.authType == 'register')
        this.nav.push('auth', { authType: 'register' });
      else {
        return;
      }
      // this.navController.push(AuthPage, { authType: 'register' });
    }, 300);
  }

  onNavigate(authType: string = '') {
    setTimeout(() => {
      switch (authType) {
        case 'login':
          this.nav.push('auth', { authType: 'login' });
          break;
        case 'register':
          this.nav.push('auth', { authType: 'register' });
          break;
        default:
          this.nav.push('auth', { authType: 'reset' });
      }
    });
  }
}
