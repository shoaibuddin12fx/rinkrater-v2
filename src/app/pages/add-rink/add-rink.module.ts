import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddRinkPageRoutingModule } from './add-rink-routing.module';

import { AddRinkPage } from './add-rink.page';
import { RinkFormComponent } from 'src/app/components/rink-form/rink-form.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { RinkFormModule } from 'src/app/components/rink-form/rink-form.component.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, AddRinkPageRoutingModule, ComponentsModule, RinkFormModule],
  providers: [],
  declarations: [AddRinkPage],
})
export class AddRinkPageModule {}
