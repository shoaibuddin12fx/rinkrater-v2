import { Component, Injector, OnInit } from '@angular/core';
import { Keyboard } from '@capacitor/keyboard';
import { AlertController } from '@ionic/angular';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-add-rink',
  templateUrl: './add-rink.page.html',
  styleUrls: ['./add-rink.page.scss'],
})
export class AddRinkPage extends BasePage implements OnInit {
  private $uid: string;

  constructor(
    private alertCtrl: AlertController,
    // private firebaseAuth: FirebaseAuth,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {}

  ionViewDidLoad() {
    // this.firebaseAuth.subscribe(
    //   (auth) => (this.$uid = auth.uid),
    //   (error) => this.utilsService.alert(error)
    // );
  }

  ionViewDidLeave() {
    Keyboard.hide();
  }

  submit(form) {
    this.rinksService.createNewRink(this.$uid, form.value).subscribe(
      async (key: string) => {
        form.reset();

        let alert = await this.alertCtrl.create({
          header: 'Added successfully',
          subHeader:
            'Your Rink has been added. We will verify if it was previously added.',
          buttons: ['Dismiss'],
        });
        alert.present();

        // let confirm = this.alertCtrl.create({
        //     title: 'Add a Review',
        //     message: 'Do you want to be the first Reviewer of this rink?',
        //     buttons: [
        //         {
        //             'text': 'No Thanks.',
        //             handler: () => {
        //                 console.log('cancelled');
        //             }
        //         },
        //         {
        //             'text': 'Absolutely!',
        //             handler: () => {
        //                 console.log('rink key', key);
        //                 this.navController.push(AddReviewPage, { key: key });
        //             }
        //         },
        //     ]
        // });
        // confirm.present();
      },
      (error) => this.utilsService.alert(error)
    );
  }
}
