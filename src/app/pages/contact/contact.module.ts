import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactPageRoutingModule } from './contact-routing.module';

import { ContactPage } from './contact.page';
import { SocialComponent } from 'src/app/components/social/social.component';
import { BannerComponent } from 'src/app/components/banner/banner.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ContactPageRoutingModule],
  declarations: [ContactPage, SocialComponent, BannerComponent],
})
export class ContactPageModule {}
