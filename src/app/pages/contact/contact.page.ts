import { Component, Injector, OnInit } from '@angular/core';
import { FAQS } from 'src/app/shared/constants';
import { BasePage } from '../base/base.page';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage extends BasePage implements OnInit {
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  openEmail(subject: string) {
    if (this.platform.is('cordova')) {
      let email = {
        to: 'senan@rinkrater.com',
        subject: subject,
        body: '',
        isHtml: true,
      };
      this.emailComposer.open(email);
    }
  }

  goSearch() {
    setTimeout(
      () =>
        this.nav.setRoot(
          'search',
          {}
          // { animate: true, direction: 'back' }
        ),
      300
    );
  }

  openPage() {
    this.platform
      .ready()
      .then(() => this.utility.inAppBrowser.create(FAQS, '_blank').show());
  }
}
