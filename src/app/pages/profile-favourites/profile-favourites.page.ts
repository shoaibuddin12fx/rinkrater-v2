import { Component, Injector, OnInit } from '@angular/core';
import { Rink } from 'src/app/shared/rink';
import { BasePage } from '../base/base.page';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-profile-favourites',
  templateUrl: './profile-favourites.page.html',
  styleUrls: ['./profile-favourites.page.scss'],
})
export class ProfileFavouritesPage extends BasePage implements OnInit {
  user: any;
  rinks: Rink[];
  accordion: Array<{ toggle: boolean; iconRight: string }> = [];
  private previous: number;

  constructor(private injector: Injector, private callNumber: CallNumber) {
    super(injector);
    this.user = this.nav.getQueryParams()?.user;
  }

  private isNotEmpty(value: string): boolean {
    return value && value.trim() != '';
  }

  ngOnInit() {
    if (this.user) {
      this.rinksService.getRinkFavByUid(this.user.$key).subscribe(
        (rinks) => {
          this.rinks = rinks;
          this.rinks.forEach((rink) =>
            this.accordion.push({ toggle: false, iconRight: 'add' })
          );
        },
        (error: any) => this.utilsService.alert(error)
      );
    }
  }

  toggle(idx: number) {
    this.accordion[idx].toggle = !this.accordion[idx].toggle;
    this.accordion[idx].iconRight =
      this.accordion[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0) this.previous = idx;
    else {
      if (this.previous != idx) {
        this.accordion[this.previous].toggle = false;
        this.accordion[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }

  call(rink: Rink): void {
    if (this.isNotEmpty(rink.phone)) {
      this.callNumber.callNumber(rink.phone, true).then(
        () => console.log('Launched dialer!'),
        (error: any) => this.utilsService.alert(error)
      );
    } else
      this.utilsService.alert(
        'This Rink does not provide a phone number',
        'Call Rink'
      );
  }

  web(rink: Rink): void {
    this.platform.ready().then(() => {
      if (this.isNotEmpty(rink.website))
        this.utility.inAppBrowser.create(rink.website, '_blank').show();
      else
        this.utilsService.alert(
          'This Rink does not provide a link to a website',
          'Rink Website'
        );
    });
  }

  map(rink): void {
    const options = { timeout: 10000, enableHighAccuracy: true };

    this.utilsService.showLoading('Loading...', 'Map Navigator');

    // Geolocation.getCurrentPosition(options).then(
    //   (resp) => {
    //     const lat = resp.coords.latitude;
    //     const lng = resp.coords.longitude;
    //     const options: LaunchNavigatorOptions = {
    //       start: lat + ', ' + lng,
    //     };

    //     this.utilsService.hideLoading();

    //     LaunchNavigator.navigate([rink.lat, rink.long], options).then(
    //       (success) => console.log('Launched navigator'),
    //       (error) => console.log('Error launching navigator', error)
    //     );
    //   },
    //   (err) => {
    //     console.log('Geolocation err: ' + JSON.stringify(err));
    //   }
    // );
  }

  goInfoDetails(rink: Rink): void {
    this.nav.push('rink-info-details', { rink: rink });
  }
}
