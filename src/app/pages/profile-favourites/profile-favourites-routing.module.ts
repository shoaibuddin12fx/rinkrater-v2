import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileFavouritesPage } from './profile-favourites.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileFavouritesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileFavouritesPageRoutingModule {}
