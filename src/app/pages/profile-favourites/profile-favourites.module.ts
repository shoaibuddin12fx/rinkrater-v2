import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileFavouritesPageRoutingModule } from './profile-favourites-routing.module';

import { ProfileFavouritesPage } from './profile-favourites.page';
import { SocialComponent } from 'src/app/components/social/social.component';
import { BannerComponent } from 'src/app/components/banner/banner.component';
import { RinkNamePipe } from 'src/app/shared/pipes/rink-name.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileFavouritesPageRoutingModule,
  ],
  declarations: [
    ProfileFavouritesPage,
    SocialComponent,
    BannerComponent,
    RinkNamePipe,
  ],
})
export class ProfileFavouritesPageModule {}
