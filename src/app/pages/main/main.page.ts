import { Component, Injector, OnInit } from '@angular/core';
import { Browser, OpenOptions } from '@capacitor/browser';
import { PRIVACY_POLICY, TERMS_OF_USE } from 'src/app/shared/constants';
import { BasePage } from '../base/base.page';
import firebase from 'firebase/compat/app';
import 'firebase/auth';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage extends BasePage implements OnInit {
  slideOptions: any = {
    loop: true,
    pager: true,
    autoplay: 2000,
  };
  ref: any;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() { }

  authRedirect(type: string): void {
    setTimeout(() => this.nav.push('auth', { authType: type }), 300);
  }

  async facebookLogin() {
    // show native spinner
    this.utility.showLoading('Loading...');

    let authRes = await this.auth.facebookLogin();

    console.log(authRes);
    // const promise = firebase.database().ref().child(`/users/${authRes.user.uid}`).once('value');
    // return Observable.fromPromise(promise);
    const user = {
      email: authRes.user.uid || '',
      alias: 'Hockey Fan',
      fullName: authRes.user.displayName,
      photo: {
        src: authRes.user.photoURL || 'assets/img/common/silhoutte.png',
        raw: null
      },
      coverPhoto: {
        src: 'assets/img/profile/myprofile_bkgrnd_photo.png',
        raw: null
      },
      provider: 'facebook'
    };
    this.userService.createUser(authRes.user.uid, user);

    // show success toast to user
    this.toastService.show('Facebook logged in successfully!');
  }

  async twitterLogin() {
    // show native spinner
    this.utility.showLoading('Loading...Authenticating Twitter');

    let authRes = await this.auth.twitterLogin();

    console.log(authRes);
    // const promise = firebase.database().ref().child(`/users/${authRes.user.uid}`).once('value');
    // return Observable.fromPromise(promise);
    const user = {
      email: authRes.user.uid || '',
      alias: 'Hockey Fan',
      fullName: authRes.user.displayName,
      photo: {
        src: authRes.user.photoURL || 'assets/img/common/silhoutte.png',
        raw: null
      },
      coverPhoto: {
        src: 'assets/img/profile/myprofile_bkgrnd_photo.png',
        raw: null
      },
      provider: 'facebook'
    };
    this.userService.createUser(authRes.user.uid, user);

    // show success toast to user
    this.toastService.show('Facebook logged in successfully!');
    // Observable
    //   .fromPromise(this.platform.ready())
    //   .flatMap(
    //     () => this.auth.twitterNativeLogin()
    //   )
    //   .flatMap(
    //     (response: any) => {
    //       const token = response.token;
    //       const secret = response.secret;

    //       console.log('token: ', token);
    //       console.log('secret: ', secret);

    //       return this.auth.twitterFirebaseLogin(token, secret);
    //     }
    //   )
    //   .flatMap(
    //     (response: any) => {
    //       authRes = response;

    //       console.log('authRes: ', authRes);

    //       const promise = this.ref.child(`/users/${authRes.uid}`).once('value');
    //       // return Observable.fromPromise(promise);
    //     }
    //   )
    //   .subscribe(
    //     (snapshot: any) => {

    //       const user = {
    //         email: authRes.email || '',
    //         alias: 'Hockey Fan',
    //         fullName: authRes.displayName,
    //         photo: authRes.photoURL,
    //         coverPhoto: 'assets/img/profile/myprofile_bkgrnd_photo.png',
    //         provider: 'twitter'
    //       };
    //       if (!snapshot.val())
    //         this.userService.createUser(authRes.uid, user);
    //       this.toastService.show('Twitter logged in successfully!');
    //     },
    //     (error: any) => {
    //       console.log('error: ', error);
    //       this.displayErrorHideSpinner(error);
    //     },
    //     () => this.utility.hideLoading()
    //   );
  }

  openInAppBrowser(type): void {
    const isPrivacy: boolean = type == 'privacy';
    const link: OpenOptions = isPrivacy ? PRIVACY_POLICY : TERMS_OF_USE;
    this.platform.ready().then(() => Browser.open(link));
  }

  private displayErrorHideSpinner(error: any): void {
    const message = error.message || 'Something went wrong';
    this.toastService.show(message);
    this.utility.hideLoading();
  }
}
