import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Platform } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnChanges {
  @Input() ref: string;
  hasBanner: boolean = false;
  src: string = '';
  link: any;

  constructor(
    private platform: Platform,
    private angularFireDatabase: AngularFireDatabase
  ) {}

  openRinkRater() {
    this.platform.ready().then(() => {
      Browser.open(this.link);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['ref']) {
      //   this.angularFireDatabase.object(`/banners`)((banners: any) => {
      //     console.log(banners);
      //     const split: Array<string> = this.ref.split('/');
      //     split.shift();
      //     console.log(split);
      //     if (split.length == 2) {
      //       this.hasBanner =
      //         !!banners &&
      //         !!banners[split[0]] &&
      //         !!banners[split[0]][split[1]] &&
      //         !!banners[split[0]][split[1]].src;
      //       this.link = banners[split[0]][split[1]].link;
      //     } else {
      //       this.hasBanner =
      //         !!banners && !!banners[split[0]] && !!banners[split[0]].src;
      //       this.link = banners[split[0]].link;
      //     }
      //     if (this.hasBanner) {
      //       if (split.length == 2) this.src = banners[split[0]][split[1]].src;
      //       else this.src = banners[split[0]].src;
      //     }
      //     console.log(this.hasBanner);
      //   });
      // }
    }
  }
}
