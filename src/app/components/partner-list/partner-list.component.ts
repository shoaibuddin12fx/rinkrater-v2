import {
  Component,
  Injector,
  Input,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base/base.page';
import { BASE_URL } from 'src/app/shared/constants';
import { PartnerFilterPipe } from './partner-filter.pipe';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss'],
})
export class PartnerListComponent extends BasePage implements OnInit {
  @Input() query: string;
  partners: any;
  isCordova: boolean;

  constructor(
    private angularFireDatabase: AngularFireDatabase,
    private injector: Injector
  ) {
    super(injector);
    this.isCordova = this.platform.is('cordova');
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes['query']) {
      const query = changes['query'].currentValue;
      // this.angularFireDatabase.list(`/partners`).subscribe((data: any) => {
      //   this.partners = new PartnerFilterPipe().transform(data, query);
      // });
    }
  }

  onInAppBrowser(key: string) {
    setTimeout(() => {
      this.platform.ready().then(() => {
        if (this.isCordova)
          this.utility.inAppBrowser
            .create(`${BASE_URL}/${key}`, '_blank')
            .show();
      });
    }, 300);
  }
}
