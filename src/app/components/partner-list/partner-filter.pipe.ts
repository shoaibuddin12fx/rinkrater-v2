import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'partnerfilter',
})
export class PartnerFilterPipe implements PipeTransform {
  transform(items: any, args?: any): any {
    let arr: Array<any> = [];
    if (items) {
      items.forEach((item) => {
        if (item.category === args) arr.push(item);
      });
      if (arr.length) return arr;
    }
    return null;
  }
}
