import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-rink-form',
  templateUrl: './rink-form.component.html',
  styleUrls: ['./rink-form.component.scss'],
})
export class RinkFormComponent implements OnInit {
  
  form: FormGroup;
  states = {
    canada: [],
    usa: [],
  };
  constructor(private fb: FormBuilder) {
    this.states.canada = [
      'AB',
      'BC',
      'MB',
      'NB',
      'NL',
      'NS',
      'NT',
      'ON',
      'PE',
      'QC',
      'SK',
      'YT',
    ];
    this.states.usa = [
      'AK',
      'AL',
      'AR',
      'AZ',
      'CA',
      'CO',
      'CT',
      'DC',
      'DE',
      'FL',
      'GA',
      'HI',
      'IA',
      'ID',
      'IL',
      'IN',
      'KS',
      'KY',
      'LA',
      'MA',
      'MD',
      'ME',
      'MI',
      'MN',
      'MO',
      'MS',
      'MT',
      'NC',
      'ND',
      'NE',
      'NH',
      'NJ',
      'NM',
      'NV',
      'NY',
      'OH',
      'OK',
      'OR',
      'PA',
      'RI',
      'SC',
      'SD',
      'TN',
      'TX',
      'UT',
      'VA',
      'VT',
      'WA',
      'WI',
      'WV',
      'WY',
    ];

    console.log(this.states);
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      address: [''],
      address2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: [''],
      country: ['canada', Validators.required],
      phone: [''],
      website: [''],
      description: [''],
    });
  }

  changed() {
    this.form.patchValue({ state: '' });
  }

  isErrorVisible(field: string, error: string) {
    return (
      this.form.controls[field].dirty &&
      this.form.controls[field].errors &&
      this.form.controls[field].errors[error]
    );
  }

  reset() {
    this.form.reset();
  }

  get valid() {
    return this.form.valid;
  }

  get value() {
    return this.form.value;
  }
}
