import { Component, Injector, Input, OnInit } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Social } from 'src/app/services/social';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppRate } from '@ionic-native/app-rate/ngx';
import { BasePage } from 'src/app/pages/base/base.page';
import {
  ANDROID_MARKET,
  FACEBOOK_SOCIAL,
  IOS_MARKET,
  TWITTER_SOCIAL,
} from 'src/app/shared/constants';

declare var cordova: any;

@Component({
  selector: 'social',
  templateUrl: 'social.component.html',
  animations: [
    trigger('shareFacebookState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('shareTwitterState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('shareGoogleState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('rateItunesState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('rateGooglePlayState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('followFbState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
    trigger('followTwitterState', [
      state('mouseup', style({ transform: 'scale(1)' })),
      state('mousedown', style({ transform: 'scale(2)' })),
      transition('mouseup <=> mousedown', animate('200ms ease-in-out')),
    ]),
  ],
})
export class SocialComponent extends BasePage implements OnInit {
  @Input() shareTitle: string = 'SHARE RINK RATER!';
  @Input() hasRate: boolean = true;
  @Input() hasFollow: boolean = true;
  @Input() photosArray: Array<string> = [];
  shareFacebookState: string;
  shareTwitterState: string;
  shareGoogleState: string;
  rateItunesState: string;
  rateGooglePlayState: string;
  followFbState: string;
  followTwitterState: string;

  constructor(private social: Social, private injector: Injector) {
    super(injector);
  }
  ngOnInit() {}

  shareVia(social: string) {
    this.platform.ready().then(() => {
      if (
        (social === 'facebook' || social === 'twitter') &&
        this.device.platform === 'iOS'
      ) {
        this.social
          .shareVia(social, this.photosArray)
          .subscribe(this.utility.dialogs.alert, this.utility.dialogs.alert);
      } else {
        this.social.checkAppAvailability(social).subscribe(
          () => {
            this.social
              .shareVia(social, this.photosArray)
              .subscribe(console.log, this.utility.dialogs.alert);
          },
          () => {
            this.utility.dialogs.alert(
              'The app you are trying to share with is not available please download the app. Thanks!'
            );
            cordova.plugins.market.open(
              this.social.apps[this.device.platform][social].id
            );
          }
        );
      }
    });
  }

  followUs(socialType: string) {
    this.platform.ready().then(() => {
      switch (socialType) {
        case 'facebook':
          this.utility.inAppBrowser.create(FACEBOOK_SOCIAL, '_blank').show();
          break;
        case 'twitter':
          this.utility.inAppBrowser.create(TWITTER_SOCIAL, '_blank').show();
          break;
      }
    });
  }

  onAppRate(store: string) {
    this.utility.appRate.preferences.storeAppURL = {
      ios: '1203876613',
      android: 'market://details?id=com.northpoledesign.rinkrater',
    };

    this.platform.ready().then(() => {
      if (
        (this.platform.is('ios') && store == 'ios') ||
        (this.platform.is('android') && store == 'android')
      ) {
        this.utility.appRate.promptForRating(true);
      } else {
        if (store == 'ios')
          this.utility.inAppBrowser.create(IOS_MARKET, '_blank').show();
        else this.utility.inAppBrowser.create(ANDROID_MARKET, '_blank').show();
      }
    });
  }
}
