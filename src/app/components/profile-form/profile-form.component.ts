import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
})
export class ProfileFormComponent implements OnChanges {
  @Input() user: any;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      alias: ['', Validators.required],
      photo: ['', Validators.required],
      coverPhoto: ['', Validators.required],
      password: [''],
      confirmPassword: [''],
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['user']) this.form.patchValue(changes['user'].currentValue);
  }

  isErrorVisible(field: string, error: string) {
    return (
      this.form.controls[field].dirty &&
      this.form.controls[field].errors &&
      this.form.controls[field].errors[error]
    );
  }

  reset() {
    this.form.reset();
  }

  get valid() {
    return this.form.valid;
  }

  get value() {
    return this.form.value;
  }
}
