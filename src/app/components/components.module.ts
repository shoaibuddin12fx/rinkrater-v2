import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { ReviewCategoriesComponent } from './review-categoriees/review-categories.component';
import { IonicModule } from '@ionic/angular';
import { ReviewCountPipe } from './review-categoriees/pipes/review-count.pipe';
import { MyHockeyRankingsComponent } from './my-hockey-rankings/my-hockey-rankings.component';

@NgModule({
  imports: [CommonModule, IonicModule],
  providers: [ReviewCountPipe],
  declarations: [
    BannerComponent,
    ReviewCategoriesComponent,
    ReviewCountPipe,
    MyHockeyRankingsComponent,
  ],
  exports: [
    BannerComponent,
    ReviewCategoriesComponent,
    ReviewCountPipe,
    MyHockeyRankingsComponent,
  ],
})
export class ComponentsModule {}
