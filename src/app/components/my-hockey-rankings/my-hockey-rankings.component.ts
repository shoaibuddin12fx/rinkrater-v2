import { Component, OnInit } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { HOCKEY_RANKINGS_URL } from 'src/app/shared/constants';

@Component({
  selector: 'my-hockey-rankings',
  templateUrl: './my-hockey-rankings.component.html',
  styleUrls: ['./my-hockey-rankings.component.scss'],
})
export class MyHockeyRankingsComponent implements OnInit {

  constructor() { }

  ngOnInit() { }


  onInAppBrowser() {
    Browser.open(HOCKEY_RANKINGS_URL);
  }
}
