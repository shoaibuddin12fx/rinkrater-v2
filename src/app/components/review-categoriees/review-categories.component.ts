import { Component, EventEmitter, Injector, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { BasePage } from 'src/app/pages/base/base.page';
import { ReviewsService } from 'src/app/services/review';
import { Review } from 'src/app/shared/review';
import { Rink } from 'src/app/shared/rink';
import { ReviewCountPipe } from './pipes/review-count.pipe';

@Component({
  selector: 'review-categories',
  templateUrl: './review-categories.component.html',
  styleUrls: ['./review-categories.component.scss'],
})
export class ReviewCategoriesComponent extends BasePage implements OnInit {

  @Input() rink: Rink;
  @Output() onEmitCategory = new EventEmitter<string>();
  reviews: Review[];
  accordion: Array<{ category: string, description: string, leftImage: string, toggle: boolean, iconRight: string }>;
  staggeringAccordion: Array<{ category: string, leftImage: string, toggle: boolean, iconRight: string }> = [];
  next: number = 0;
  private previous: number;
  
  constructor(
    injector: Injector,
    public reviewsService: ReviewsService,
    private reviewcount: ReviewCountPipe
  ) {
    super(injector)
    this.accordion = [
      {
        category: 'FIRST IMPRESSIONS',
        description: 'What do you think of the overall facility? Give your initial thoughts here.',
        leftImage: 'assets/img/common/icon_comments.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'HOME TEAM INSIGHTS',
        description: 'From a Home Team perspective, provide incoming teams with the Inside Scoop! Is there an ATM? Local hotspots?',
        leftImage: 'assets/img/common/icon_local_tips.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'PRO SHOP',
        description: 'Does this Rink have a Pro Shop? Give us the low down on what they’ve got!',
        leftImage: 'assets/img/common/icon_proshop.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'SKATE SHARPENING',
        description: 'Does this rink / arena offer skate sharpening? Anyone in particular to go see?',
        leftImage: 'assets/img/common/icon_sk8_sharpening.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'CONCESSIONS',
        description: 'Tell us what’s good! How’s the variety and prices?',
        leftImage: 'assets/img/common/icon_concessions.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'RINK TEMPERATURE',
        description: 'Let your fellow Hockey Parents know what they can expect. Is it comfy or Arctic?',
        leftImage: 'assets/img/common/icon_rink_temps.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'FACILITY OVERVIEW',
        description: 'How’s  the overall facility? Is it well maintained and up to date or…not so much.',
        leftImage: 'assets/img/common/icon_rink.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'WI-FI',
        description: 'Is there Wi-Fi available? Is it password protected? List the password here.',
        leftImage: 'assets/img/common/icon_wifi.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'LIVE STREAMING',
        description: 'Does this facility offer Live Streaming services for those who couldn’t make it to the game? Which one?',
        leftImage: 'assets/img/common/icon_live_stream.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'ICE CONDITIONS',
        description: 'How’s the ice? Good condition or rutty? Hard or soft ice? Sloped anywhere?',
        leftImage: 'assets/img/common/icon_ice.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'WARM UP AREAS',
        description: 'Is there room or areas for team warm ups?',
        leftImage: 'assets/img/common/icon_warmup_area.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'RESTROOMS',
        description: 'Are they in good shape and well maintained? Clean?',
        leftImage: 'assets/img/common/icon_restroom_area.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'PARKING',
        description: 'Plenty of parking or tough to find a spot?',
        leftImage: 'assets/img/common/icon_parking.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'LOCKER ROOMS',
        description: 'Are the Locker Rooms tiny closets or plenty of room for a team & gear?',
        leftImage: 'assets/img/common/icon_lockerroom.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'SEATING / BEST VIEWING SPOTS',
        description: 'What kind of seating is there? Cold Metal or Wood? Any great viewing locations?',
        leftImage: 'assets/img/common/icon_seating.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'SLED HOCKEY',
        description: 'Is the rink / arena accessible?',
        leftImage: 'assets/img/common/icon_sled_hockey.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'REF\'S ROOM',
        description: 'Officials can inform one other about Ref-Specific Rink Information. \nex: Where is the Ref&#39;s Locker Room? Are there showers? etc.',
        leftImage: 'assets/img/common/icon_refs_icon.png',
        toggle: false,
        iconRight: 'add'
      },
      {
        category: 'ROLLER HOCKEY',
        description: 'Does this facility offer Roller Hockey?',
        leftImage: 'assets/img/common/icon_roller_hockey.png',
        toggle: false,
        iconRight: 'add'
      },
    ];
  }

  ngOnInit() {
    this.onAnimateStateDone();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.rink) {
      this.reviewsService
        .getAllReviewsForRink(this.rink.$key)
        .subscribe((response) => {
          console.log(response);
          this.reviews = response;
        }, alert);
    }
  }

  ngOnDestroy() {
    this.staggeringAccordion = [];
    this.next = 0;
  }

  onAnimateStateDone() {
    if (this.next < this.accordion.length)
      this.staggeringAccordion.push(this.accordion[this.next++]);
  }

  onAddReviewOnSamePage(idx, ev) {
    this.toggle(idx, ev);
    this.onEmitCategory.emit(ev);
  }

  toggle(idx: number, category: string) {

    console.log(category);
    let hasReview = this.reviewcount.transform(this.reviews, category);

    if (hasReview === 'Be the First to Review!') {
      // const activeView = this.nav.getActive();
      // if (activeView.component == '') {
      //   this.onEmitCategory.emit(category);
      // } else {
        // setTimeout(() => {
          this.nav.push('add-review', { rink: this.rink, category: category })
        // }, 300);
      // }
      return;
    }

    this.accordion[idx].toggle = !this.accordion[idx].toggle;
    this.accordion[idx].iconRight = this.accordion[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0)
      this.previous = idx;
    else {
      if (this.previous != idx) {
        this.accordion[this.previous].toggle = false;
        this.accordion[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }

}
