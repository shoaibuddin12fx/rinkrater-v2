import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reviewcount'
})
export class ReviewCountPipe implements PipeTransform {

  transform(items: any, args?: any): any {
    let idx = 0;
    if (items) {
      items.forEach((item) => {
        if ((item.category === args))
          idx++;
      });
      return idx ? idx > 1 ? idx + ' Reviews' : idx + ' Review' : 'Be the First to Review!';
    }
    return 'Be the First to Review!';
  }
}
