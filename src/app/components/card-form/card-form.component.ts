import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss'],
})
export class CardFormComponent implements OnChanges {
  @Input() parentCard: any;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      kids: '',
      farthest: '',
      training: '',
      campsAndClinics: '',
      sponsored: '',
      story: '',
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['parentCard'].currentValue)
      this.form.patchValue(changes['parentCard'].currentValue);
  }

  reset() {
    this.form.reset();
  }

  get valid() {
    return this.form.valid;
  }

  get value() {
    return this.form.value;
  }

  ngOnInit() {}
}
