import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RinkNamePipe } from 'src/app/shared/pipes/rink-name.pipe';
import { IonicModule } from '@ionic/angular';
import { ReviewCountPipe } from './pipes/review-count.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [RinkNamePipe],
    exports: [
        RinkNamePipe
    ]
})
export class SharedModule { }