export class Review {

    constructor(
        public $key: string,
        public category: string,
        public comment: string,
        public created_at: number,
        public created_by: string,
        public rinkId: string,
        public updated_at: number,
        public updated_by: string,
        public userId: string
    ) { }

    static fromJson({ $key, category, comment, created_at, created_by, rinkId, updated_at, updated_by, userId }) {
        return new Review($key, category, comment, created_at, created_by, rinkId, updated_at, updated_by, userId);
    }

    static fromJsonArray(json: any[]): Review[] {
        return json.map(Review.fromJson);
    }

}
