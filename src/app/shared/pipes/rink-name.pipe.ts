import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rinkname'
})
export class RinkNamePipe implements PipeTransform {

  transform(items: any, ...args: unknown[]): unknown {
    if (!items)
      return items;

    if (items.indexOf('-') == -1)
      return items;
    else
      return items.substring(0, items.indexOf('-'));
  }

}
