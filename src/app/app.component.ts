import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { Keyboard } from '@capacitor/keyboard';
import { Platform } from '@ionic/angular';
import { EventsService } from './services/events.service';
import { NavService } from './services/nav.service';
import { UtilityService } from './services/utility.service';
import { GEAR_STORE, REF_HAND_SIGNALS, NORTH_POLE_URL } from './shared/constants';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import 'firebase/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  animations: [
    trigger('itemState', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate('100ms cubic-bezier(0.175, 0.885, 0.32, 1.275)')
      ])
    ])
  ]
})

export class AppComponent {
  rootPage: any;
  state: string = 'out';

  pages: Array<{ title: string, component: any }>;
  staggeringPages: Array<{ title: string, component: any }> = [];
  next: number = 0;

  constructor(
    private platform: Platform,
    private events: EventsService,
    private utilsService: UtilityService,
    private nav: NavService,
    private firebaseAuth: AngularFireAuth) {
    this.initializeApp();

    // navigation
    this.pages = [
      { title: 'PROFILE SETTINGS', component: '' },
      { title: 'RINK RATER PRO SHOP', component: '' },
      { title: 'REFEREE HAND SIGNALS', component: '' },
      { title: 'NEARBY', component: '' },
      { title: 'ABOUT US', component: '' },
      { title: 'RATE THIS APP!', component: '' },
      { title: 'CONTACT US', component: '' },
      { title: 'LOG OUT', component: '' }
    ];
  }

  async initializeApp() {

    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        Keyboard.setAccessoryBarVisible({ isVisible: false });
        // Splashscreen.hide();
      }
      return this.firebaseAuth.authState.subscribe((data) => {
        const isEmailVerified: boolean = (!!data && data.emailVerified);
        const isTwitter: boolean = (!!data && !!data.providerData);
        const isFacebook: boolean = (!!data && !!data.providerId);

        if (isEmailVerified || isTwitter || isFacebook) {
          this.nav.setRoot('search');
        } else if (!!data && !data.emailVerified) {
          this.nav.setRoot('main');

          const message: string = 'Before you use Rink Rater, you must verify your email address. ' +
            'Please check your Inbox (or possibly Junk Mail) to verify your account and start Rating Rinks!';

          const title: string = 'Verify';
          const buttons: Array<string> = ['Okay', 'Re-send Verification'];

          this.utilsService.confirm(message, title, buttons)
            .then(async (index) => {
              console.log(index);
              if (index == 2) {
                let user = await this.firebaseAuth.currentUser;
                user.sendEmailVerification().then(
                  () => {
                    this.utilsService.alert('Email verification has been sent!');
                    this.firebaseAuth.signOut();
                  },
                  console.log
                );
              } else {
                console.log('choose 1');
                this.firebaseAuth.signOut();
              }
            });
        } else {
          this.nav.setRoot('main');
        }
      })

    });

  }

  itemStateDone() {
    if (this.next < this.pages.length && this.state == 'in')
      this.staggeringPages.push(this.pages[this.next++]);
  }

  openPage(page) {
    if (page.title === 'RINK RATER PRO SHOP') {
      this.platform.ready().then(
        () => Browser.open(GEAR_STORE)

      );
    } else if (page.title === 'REFEREE HAND SIGNALS') {
      this.platform.ready().then(
        () => Browser.open(REF_HAND_SIGNALS)
      );
    } else if (page.title === 'RATE THIS APP!') {
      // AppRate.preferences.storeAppURL = {
      //   ios: '1203876613',
      //   android: 'market://details?id=com.northpoledesign.rinkrater',
      // };
      // AppRate.promptForRating(true);
    } else if (page.title === 'LOG OUT') {
      // this.firebaseAuth.logout();
      // window.location.reload();
    } else {
      this.nav.setRoot(page.component, {}, 'right');
    }
  }

  menuClosed() {
    console.log('menu closed');
    this.events.publish('menu:closed', '');
    this.state = 'out';
    this.staggeringPages = [];
    this.next = 0;
    console.log(this.staggeringPages);
  }

  menuOpened() {
    this.events.publish('menu:opened', '');
    this.state = 'in';
    this.itemStateDone();
  }

  showNpd() {
    this.platform.ready().then(
      () => Browser.open(NORTH_POLE_URL)
    );
  }
}
