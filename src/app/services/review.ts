import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, Subject } from 'rxjs';
import { Review } from '../shared/review';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  ref: any;

  constructor(private db: AngularFireDatabase) {
    this.ref = db.database.ref();
  }

  getAllReviewsForRink(rinkId): Observable<Review[]> {
    const reviewsPerRink$ = this.db.list(`rink-reviews/${rinkId}`); // reviewsPerRink
    return new Observable<Review[]>();

    // return reviewsPerRink$
    //   .map((rspr) => rspr.map((rpr) => this.db.object(`reviews/${rpr.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  getAllReviewsForUser(userId): Observable<Review[]> {
    const reviewsPerRink$ = this.db.list(`user-reviews/${userId}`); // reviewsPerUser
    return new Observable<Review[]>();
    // return reviewsPerRink$
    //   .map((rspr) => rspr.map((rpr) => this.db.object(`reviews/${rpr.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  createNewReview(review: any, photo: any = null): Observable<any> {
    const subject = new Subject();

    this.ref
      .child('/counts/reviews')
      .once('value')
      .then((snapshot) => {
        let dataToSave = {};
        let reviewToSave = Object.assign({}, review);
        let newReviewKey: string;
        let count: number = snapshot.val() || 0;

        if (reviewToSave.$key) {
          newReviewKey = review.$key;
          delete reviewToSave.$key;
          delete reviewToSave.$exists;
        } else newReviewKey = this.ref.child('reviews').push().key;

        dataToSave[`reviews/${newReviewKey}`] = reviewToSave;
        dataToSave[`user-reviews/${review.userId}/${newReviewKey}`] = true;
        dataToSave[`rink-reviews/${review.rinkId}/${newReviewKey}`] = true;
        dataToSave[`counts/reviews`] = count + 1;

        if (!!photo) {
          let photoToSave = Object.assign({}, photo);
          let newPhotoKey = this.ref.child('photos').push().key;

          dataToSave[`photos/${newPhotoKey}`] = photoToSave;
          dataToSave[`user-photos/${review.userId}/${newPhotoKey}`] = true;
          dataToSave[`rink-photos/${review.rinkId}/${newPhotoKey}`] = true;
        }
        return this.ref.update(dataToSave);
      })
      .then(
        () => {
          subject.next();
          subject.complete();
        },
        (err) => {
          subject.error(err);
          subject.complete();
        }
      );

    return subject.asObservable();
  }

  // private firebaseUpdate(dataToSave: any, key: string): Observable<any> {
  //     const subject = new Subject();
  //
  //     this.ref.update(dataToSave)
  //         .then(
  //             () => {
  //                 subject.next(key);
  //                 subject.complete();
  //             },
  //             err => {
  //                 subject.error(err);
  //                 subject.complete();
  //             }
  //         );
  //
  //     return subject.asObservable();
  // }
}
