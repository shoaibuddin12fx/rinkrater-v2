import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import 'firebase/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private auth: AngularFireAuth) { }

    signIn(email: string, password: string) {
        return this.auth.signInWithEmailAndPassword(email, password);
        // return this.fromAuthPromise(this.auth.signInWithEmailAndPassword(email, password))
    }

    signUp(email: string, password: string) {
        return this.auth.signInWithEmailAndPassword(email, password);
    }

    reset(email: string) {
        return this.auth.sendPasswordResetEmail(email);
    }

    async facebookLogin() {
        var provider = new firebase.auth.FacebookAuthProvider();
        const result = await this.auth.signInWithPopup(provider);
        return result.user.linkWithPopup(provider)
    }

    // facebookFirebaseLogin(accessToken: string) {
    //     var provider = new firebase.auth.FacebookAuthProvider();
    //     provider.addScope('user_birthday');
    //     return  this.auth.signInWithPopup(provider);
    //     // console.log('facebookFirebaseLogin: ', firebase);
    //     // const facebookCredential = firebase.auth.FacebookAuthProvider.credential(accessToken);
    //     // return this.fromAuthPromise(firebase.auth().signInWithCredential(facebookCredential));
    // }

    async twitterLogin() {
        var provider = new firebase.auth.TwitterAuthProvider();
        const result = await this.auth.signInWithPopup(provider);
        return result.user.linkWithPopup(provider);
        // return this.fromAuthPromise(TwitterConnect.login());
    }

    // twitterFirebaseLogin(token: string, secret: string) {
    //     var provider = new firebase.auth.TwitterAuthProvider();
    //     return  this.auth.signInWithPopup(provider);
    //     // const twitterCredential = firebase.auth.TwitterAuthProvider.credential(token, secret);
    //     // return this.fromAuthPromise(firebase.auth().signInWithCredential(twitterCredential))
    // }

    logout() {
        this.auth.signOut();
    }
}
