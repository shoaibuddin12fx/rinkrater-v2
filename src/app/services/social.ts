import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ANDROID_MARKET, IOS_MARKET } from '../shared/constants';
import { Device } from '@ionic-native/device/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class Social {
  constructor(
    private device: Device,
    private appAvailability: AppAvailability
  ) {}

  public apps = {
    Android: {
      facebook: {
        scheme: 'com.facebook.katana',
        id: 'com.facebook.katana',
      },
      twitter: {
        scheme: 'com.twitter.android',
        id: 'com.twitter.android',
      },
      pinterest: {
        scheme: 'com.pinterest',
        id: 'com.pinterest',
      },
      gplus: {
        scheme: 'com.google.android.apps.plus',
        id: 'com.google.android.apps.plus',
      },
    },
    iOS: {
      facebook: {
        scheme: 'fb://',
        id: '284882215',
      },
      twitter: {
        scheme: 'twitter://',
        id: '333903271',
      },
      pinterest: {
        scheme: 'pinterest://',
        id: '429047995',
      },
      gplus: {
        scheme: 'gplus://',
        id: '447119634',
      },
    },
  };

  shareVia(social: string, photosArray: Array<string>) {
    const subject = new Subject<any>();
    const message = `Check out this awesome app to rate & review Hockey Rinks!\n\nApp Store: ${IOS_MARKET}\n\nGoogle Play Store: ${ANDROID_MARKET}\n\n`;

    // if (social === 'facebook' && Device.device.platform === 'iOS')
    //     return this.fromPromise(SocialSharing.shareViaFacebook('Check out this Rink Review App called Rink Rater!\nTo download:\nApp Store: http://www.itunes.com/rinkrater\nApp Store: http://www.google.com/rinkrater\n\n', photosArray || null));
    // else if (social === 'twitter' && Device.device.platform === 'iOS')
    //     return this.fromPromise(SocialSharing.shareViaTwitter('Check out this Rink Review App called Rink Rater!\nTo download:\nApp Store: http://www.itunes.com/rinkrater\nApp Store: http://www.google.com/rinkrater\n\n', photosArray || null));
    // else
    //     return this.fromPromise(SocialSharing.shareVia(this.apps[Device.device.platform][social].id, 'Check out this Rink Review App called Rink Rater!\nTo download:\nApp Store: http://www.itunes.com/rinkrater\nApp Store: http://www.google.com/rinkrater\n\n', photosArray || null));

    if (social === 'facebook' && this.device.platform === 'iOS') {
      window.plugins.socialsharing.shareViaFacebook(
        message,
        photosArray || null,
        null,
        () => {
          subject.next();
          subject.complete();
        },
        (err) => {
          console.log(err);
          subject.error(err);
          subject.complete();
        }
      );
    } else if (social === 'twitter' && this.device.platform === 'iOS') {
      window.plugins.socialsharing.shareViaTwitter(
        message,
        photosArray || null,
        null,
        () => {
          subject.next();
          subject.complete();
        },
        (err) => {
          console.log(err);
          subject.error(err);
          subject.complete();
        }
      );
    } else {
      // return this.fromPromise(SocialSharing.shareVia(this.apps[Device.device.platform][social].id, 'Check out this Rink Review App called Rink Rater!\nTo download:\nApp Store: http://www.itunes.com/rinkrater\nApp Store: http://www.google.com/rinkrater\n\n', photosArray || null));
      window.plugins.socialsharing.shareVia(
        this.apps[this.device.platform][social].id,
        message,
        photosArray || null,
        null,
        null,
        () => {
          subject.next();
          subject.complete();
        },
        (err) => {
          console.log(err);
          subject.error(err);
          subject.complete();
        }
      );
    }

    return subject.asObservable();
  }

  checkAppAvailability(social: string) {
    return this.fromPromise(
      this.appAvailability.check(this.apps[this.device.platform][social].scheme)
    );
  }

  private fromPromise(promise): Observable<any> {
    const subject = new Subject<any>();

    promise.then(
      (res) => {
        subject.next(res);
        subject.complete();
      },
      (err) => {
        subject.error(err);
        subject.complete();
      }
    );

    return subject.asObservable();
  }
}
