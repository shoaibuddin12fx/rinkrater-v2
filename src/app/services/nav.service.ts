import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@Injectable({
    providedIn: 'root'
})

export class NavService {

    options: NativeTransitionOptions = {
        direction: 'left',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
    };

    constructor(public location: Location, public router: Router,
        public activatedRoute: ActivatedRoute, private nativePageTransitions: NativePageTransitions) { }

    async setRoot(page, param = {}, direction = "right") {
        this.options.direction = direction;
        const extras: NavigationExtras = {
            queryParams: param
        };
        this.navigateTo(page, extras);
    }

    async push(page, param = {}) {

        this.options.direction = 'left';
        // await this.nativePageTransitions.slide(this.options);
        const extras: NavigationExtras = {
            queryParams: param
        };
        this.navigateTo(page, extras);
    }

    async pop() {
        return new Promise<void>(async resolve => {

            this.options.direction = 'right';
            //   await this.nativePageTransitions.slide(this.options);
            //   await this.nativePageTransitions.fade(null);
            this.location.back();
            resolve();
        });
    }

    navigateTo(link, data?: NavigationExtras) {
        console.log(link);
        this.router.navigate([link], data);
    }

    navigateToChild(link, data?: NavigationExtras) {
        data.relativeTo = this.activatedRoute;
        this.router.navigate([link], data);
    }

    getParams() {
        return this.activatedRoute.snapshot.params;
    }

    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }

}
