import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, Subject } from "rxjs";
import firebase from 'firebase/compat/app';
@Injectable({
    providedIn: 'root'
})

export class UserService {

    ref: any;

    constructor(private af: AngularFireDatabase) {
        this.ref = firebase.database().ref();
    }

    createUser(uid: string, form: any) {

        // previous work

        // let currentUserRef = this.af.database.object(`/users/${uid}`);
        // currentUserRef.set({
        //     email: form.email,
        //     alias: form.alias || 'Hockey Player',
        //     fullName: form.fullName,
        //     photo: {
        //         src: !!form.photo && form.photo.src ? form.photo.src : 'assets/img/common/silhoutte.png',
        //         raw: null
        //     },
        //     coverPhoto: {
        //         src: !!form.coverPhoto && form.coverPhoto.src ? form.coverPhoto.src : 'assets/img/profile/myprofile_bkgrnd_photo.png',
        //         raw: null
        //     }
        // });

        // new

        const subject = new Subject();

        this.ref
            .child('/counts/users')
            .once('value')
            .then((snapshot) => {
                const timestamp = new Date().getTime();
                const currentUser = {
                    email: form.email,
                    alias: form.alias || 'Hockey Fan',
                    fullName: form.fullName,
                    photo: {
                        src: !!form.photo && form.photo.src ? form.photo.src : 'assets/img/common/silhoutte.png',
                        raw: null
                    },
                    coverPhoto: {
                        src: !!form.coverPhoto && form.coverPhoto.src ? form.coverPhoto.src : 'assets/img/profile/myprofile_bkgrnd_photo.png',
                        raw: null
                    },
                    role: "user",
                    provider: form.provider || '',
                    created_at: timestamp,
                    updated_at: timestamp,
                    updated_by: ''
                };
                let count: number = snapshot.val() || 0;
                let updates: any = {};

                updates[`users/${uid}`] = currentUser;
                updates[`counts/users`] = count + 1;

                return this.ref.update(updates);
            })
            .then(
                () => {
                    subject.next();
                    subject.complete();
                },
                err => {
                    subject.error(err);
                    subject.complete();
                }
            );


        return subject.asObservable();

    }

    saveUser(userId: string, user) {

        console.log(user);

        const UserToSave = Object.assign({}, user);
        delete (UserToSave.$key);
        delete (UserToSave.$exists);

        let dataToSave = {};
        dataToSave[`users/${userId}`] = UserToSave;

        return this.firebaseUpdate(dataToSave);
    }

    getUser(uid: string) {
        return this.af.database.ref(`/users/${uid}`).get();
    }

    private firebaseUpdate(dataToSave: any): Observable<any> {
        const subject = new Subject();

        this.ref.update(dataToSave)
            .then(
                (val) => {
                    subject.next(val);
                    subject.complete();
                },
                err => {
                    subject.error(err);
                    subject.complete();
                }
            );

        return subject.asObservable();
    }
}
