import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RatingsService {
  sdkDb: any;

  constructor(private db: AngularFireDatabase) {
    this.sdkDb = db.database.ref();
  }

  getAllRatingsForRink(rinkId: string) {
    const ratingsPerRink$ = this.db.list(`rink-ratings/${rinkId}`); // ratingsPerRink
    return null;

    // return ratingsPerRink$
    //   .map((rspr) => rspr.map((rpr) => this.db.object(`ratings/${rpr.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  getRatingByUserIdAndRinkId(userId: string, rinkId: string): Observable<any> {
    const rating$ = this.db.list(`userrink-ratings/${userId}/${rinkId}`); // ratingsPerUserRink
    return null;
    // return rating$
    //   .map((data) => data.map((res) => this.db.object(`ratings/${res.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  createNewRating(rating: any): Observable<any> {
    console.log('RatingsService: createNewRating', rating);

    let dataToSave = {};
    let ratingToSave = Object.assign({}, rating);
    let newRatingKey: string;

    if (ratingToSave.$key) {
      console.log('RatingsService: has key');
      newRatingKey = rating.$key;
      delete ratingToSave.$key;
      delete ratingToSave.$exists;
    } else newRatingKey = this.sdkDb.child('ratings').push().key;

    dataToSave[`ratings/${newRatingKey}`] = ratingToSave;
    dataToSave[`user-ratings/${rating.userId}/${newRatingKey}`] = true; // ratingsPerUser
    dataToSave[`rink-ratings/${rating.rinkId}/${newRatingKey}`] = true;
    dataToSave[
      `userrink-ratings/${rating.userId}/${rating.rinkId}/${newRatingKey}`
    ] = true;

    return this.firebaseUpdate(dataToSave, newRatingKey);
  }

  private firebaseUpdate(dataToSave: any, key: string): Observable<any> {
    const subject = new Subject();

    this.sdkDb.update(dataToSave).then(
      () => {
        subject.next(key);
        subject.complete();
      },
      (err) => {
        subject.error(err);
        subject.complete();
      }
    );

    return subject.asObservable();
  }
}
