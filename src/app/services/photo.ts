import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  sdkDb: any;

  constructor(private db: AngularFireDatabase) {
    this.sdkDb = db.database.ref();
  }

  getAllPhotosForUser(userId): Observable<any> {
    const photosPerUser$ = this.db.list(`user-photos/${userId}`); // photosPerUser
    return null;
    // return photosPerUser$
    //   .map((rspr) => rspr.map((rpr) => this.db.object(`photos/${rpr.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  createNewPhoto(photo: any) {
    let dataToSave = {};
    const photoToSave = Object.assign({}, photo);
    const newPhotoKey = this.sdkDb.child('photos').push().key;

    dataToSave[`photos/${newPhotoKey}`] = photoToSave;
    dataToSave[`user-photos/${photo.userId}/${newPhotoKey}`] = true;

    return this.firebaseUpdate(dataToSave);
  }

  deletePhoto(photoId: string, userId: string) {
    let dataToDelete = {};

    dataToDelete[`photos/${photoId}`] = null;
    dataToDelete[`user-photos/${userId}/${photoId}`] = null;

    return this.firebaseUpdate(dataToDelete);
  }

  private firebaseUpdate(data: any): Observable<any> {
    const subject = new Subject();

    this.sdkDb.update(data).then(
      (value) => {
        subject.next(value);
        subject.complete();
      },
      (err) => {
        subject.error(err);
        subject.complete();
      }
    );

    return subject.asObservable();
  }
}
