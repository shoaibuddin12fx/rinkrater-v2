import { Injectable } from '@angular/core';
import { BAD_LANG } from './words';

@Injectable({
  providedIn: 'root'
})

export class BadWordsService {
  private list: Array<string>;
  private placeholder: string;
  private regex: any;
  private replaceRegex: any;

  constructor() {
    this.list = BAD_LANG.words;
    this.placeholder = '*';
    this.regex = /[^a-zA-z0-9|\$|\@]|\^/g;
    this.replaceRegex = /\w/g;
  }

  private isProfane(str: string): boolean {
    const words = str.split(' ');
    for (let j = 0; j < words.length; j++) {
      let word = words[j].toLowerCase().replace(this.regex, '');
      if (~this.list.indexOf(word)) {
        return true;
      }
    }
    return false;
  }

  private replaceWord(str: string): string {
    return str
      .replace(this.regex, '')
      .replace(this.replaceRegex, this.placeholder);
  }

  clean(str: string): string {
    return str
      .split(' ')
      .map(
        function (word) {
          return this.isProfane(word) ? this.replaceWord(word) : word;
        }.bind(this)
      )
      .join(' ');
  }
}
