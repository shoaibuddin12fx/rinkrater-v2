import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToastService {


    constructor(
        private toastCtrl: ToastController,
        private alertCtrl: AlertController
    ) {}

    async presentSuccessToast(msg) {

        const toast = await this.toastCtrl.create({
          duration: 5000,
          position: 'top',
          cssClass: 'successToast'
        });
    
        toast.present();
      }
    
      async presentFailureToast(msg) {
        const toast = await this.toastCtrl.create({
          duration: 5000,
          position: 'top',
          cssClass: 'failureToast'
        });
    
        toast.present();
      }
    
      async show(msg) {
        const toast = await this.toastCtrl.create({
          message: msg,
          duration: 5000,
          position: 'bottom'
        });
        toast.present();
      }
}


