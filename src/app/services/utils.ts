import { Injectable } from '@angular/core';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private loading: any;

  constructor(
    private alertController: AlertController,
    private loadingController: LoadingController,
  ) { }

  private checkMessage(value: any): any {
    return typeof value != 'string' ? JSON.stringify(value) : value;
  }

  alert(msg, title = 'Alert', buttonName: any = 'Ok'): Promise<any> {
    return new Promise(async resolve => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: title,
        message: msg,
        buttons: [
          {
            text: buttonName,
            cssClass: 'secondary',
            handler: () => {
              resolve(true);
            }
          }
        ]
      });

      await alert.present();
    });

  }


  confirm(message = 'Default message', title = 'Rink Confirm!', buttonLabels: Array<string> = ['OK', 'Cancel']) {
    return new Promise(async resolve => {
      const alert = await this.alertController.create({
        header: title,
        message,
        buttons: [
          {
            text: buttonLabels[0],
            role: 'cancel',
            handler: () => {
              resolve(false);
            }
          },
          {
            text: buttonLabels[1],
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
    // else {
    //
    //     let alert = this.alertController.create({
    //         title: title,
    //         message: message,
    //         buttons: [
    //             {
    //                 text: buttonLabels[1],
    //                 handler: () => {
    //                     console.log('Disagree clicked');
    //                 }
    //             },
    //             {
    //                 text: buttonLabels[0],
    //                 handler: () => {
    //                     console.log('Agree clicked');
    //                 }
    //             }
    //         ]
    //     });
    //
    //     alert.present();
    // }
  }

  async showLoading(message: any = 'Loading...', title: string = '') {
    this.loading = await this.loadingController.create({
      spinner: 'dots',
      message,
    });
    await this.loading.present();


  }

  async hideLoading() {
    this.loading.dismiss();
  }
}
