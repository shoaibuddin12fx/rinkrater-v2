import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, Subject } from 'rxjs';
import { Rink } from '../shared/rink';

@Injectable({
  providedIn: 'root'
})
export class RinksService {
  ref: any;

  constructor(private db: AngularFireDatabase) {
    this.ref = db.database.ref();
  }

  getAllRinks(): Observable<Rink[]> {
    return new Observable<Rink[]>();
    // return this.db.list('rinks').map(Rink.fromJsonArray);
  }

  getRinkByKey(key: string) {
    return this.db.object(`rinks/${key}`);
  }

  getRinkFavByUid(userId: string): Observable<Rink[]> {
    const rinksFavPerUser$ = this.db.list(`user-rinksfav/${userId}`);
    return new Observable<Rink[]>();

    // return rinksFavPerUser$
    //   .map((rspr) => rspr.map((rpr) => this.db.object(`rinks/${rpr.$key}`)))
    //   .flatMap((fbojs) => Observable.combineLatest(fbojs));
  }

  createNewRink(userId: string, rink: any): Observable<any> {
    const promise = this.ref.child('/counts/rinks').once('value');

    return new Observable<Rink[]>();

    // return Observable.fromPromise(promise).flatMap((snapshot: any) => {
    //   const timestamp: number = new Date().getTime();
    //   let count: number = snapshot.val() || 0;

    //   rink.address = rink.address + ', ' + rink.address2;
    //   rink.active = 0;
    //   rink.altname = '';
    //   rink.altname2 = '';
    //   rink.created_at = timestamp;
    //   rink.created_by = userId;
    //   rink.lat = 0;
    //   rink.long = 0;
    //   rink.nbr = 0;
    //   rink.updated_at = timestamp;
    //   rink.updated_by = userId;

    //   delete rink.address2;

    //   const rinkToSave = Object.assign({}, rink, { userId });

    //   const newRinkKey = this.ref.child('rinks').push().key;

    //   let dataToSave = {};

    //   dataToSave['rinks/' + newRinkKey] = rinkToSave;
    //   dataToSave[`user-rinks/${userId}/${newRinkKey}`] = true;
    //   dataToSave[`counts/rinks`] = count + 1;

    //   return this.firebaseUpdate(dataToSave, newRinkKey);
    // });

    // this.ref
    //     .child('/counts/rinks')
    //     .once('value')
    //     .then((snapshot) => {
    //
    //         const timestamp: number = new Date().getTime();
    //         let count: number = snapshot.val() || 0;
    //
    //         rink.address = rink.address + ', ' + rink.address2;
    //         rink.active = 0;
    //         rink.altname = '';
    //         rink.altname2 = '';
    //         rink.created_at = timestamp;
    //         rink.created_by = userId;
    //         rink.lat = 0;
    //         rink.long = 0;
    //         rink.nbr = 0;
    //         rink.updated_at = timestamp;
    //         rink.updated_by = userId;
    //
    //         delete rink.address2;
    //
    //         const rinkToSave = Object.assign({}, rink, {userId});
    //
    //         const newRinkKey = this.ref.child('rinks').push().key;
    //
    //         let dataToSave = {};
    //
    //         dataToSave["rinks/" + newRinkKey] = rinkToSave;
    //         dataToSave[`user-rinks/${userId}/${newRinkKey}`] = true;
    //         dataToSave[`counts/rinks`] = count + 1;
    //
    //         return this.firebaseUpdate(dataToSave, newRinkKey);
    //     });
  }

  setRinkAsFavorite(userId: string, rink: any) {
    let dataToSave = {};

    dataToSave[`user-rinksfav/${userId}/${rink.$key}`] = true;

    return this.firebaseUpdate(dataToSave, rink.$key);
  }

  private firebaseUpdate(dataToSave: any, key: string): Observable<any> {
    const subject = new Subject();

    this.ref.update(dataToSave).then(
      () => {
        subject.next(key);
        subject.complete();
      },
      (err) => {
        subject.error(err);
        subject.complete();
      }
    );

    return subject.asObservable();
  }
}
